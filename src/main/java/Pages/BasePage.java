package Pages;

import net.bytebuddy.implementation.bind.annotation.Super;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.*;

import java.time.Duration;
import java.util.Random;

public class BasePage {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected Wait<WebDriver> fluentWait;
    public BasePage(WebDriver driver) {
        super();
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        this.fluentWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(5))
                .pollingEvery(Duration.ofSeconds(1)).ignoring(NoSuchElementException.class);
    }

    public void clickButton(WebElement button) {
        button.click();
    }

    public void setTextElementText(WebElement textElement, String value) {
        textElement.sendKeys(value);
    }

    public void SelectFromLOV(WebElement optionList, String value) {
        Select selectOptions = new Select(optionList);
        selectOptions.selectByValue(value);
    }

    public void pressTAB (WebDriver driver)  {
        Actions action = new Actions(driver);
        action.sendKeys(Keys.TAB).build().perform();
    }

    public static String generateEmail(){
        StringBuilder email = new StringBuilder("email");
        Random random = new Random();
        // Generating the rest
        for (int i = 0; i < 3; i++) {
            email.append(random.nextInt(1000));
        }
        email.append("@gmail.com");
        return email.toString() ;
    }
}
