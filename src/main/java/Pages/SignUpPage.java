package Pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
public class SignUpPage extends BasePage{

    public SignUpPage(WebDriver driver){
        super(driver);
    }

    @FindBy (xpath = "//a[normalize-space()='Signup / Login']")
    WebElement SignUpLink;
    @FindBy (xpath = "//input[@placeholder='Name']")
    WebElement Name;
    @FindBy (xpath = "//input[@data-qa='signup-email']")
    WebElement Email;
    @FindBy (xpath = "//button[normalize-space()='Signup']")
    WebElement SignUpBtn;
    @FindBy (id = "id_gender1")
    WebElement Gender;
    @FindBy (id = "password")
    WebElement Password;
    @FindBy (id = "days")
    WebElement Day;
    @FindBy (id = "months")
    WebElement Month;
    @FindBy (id = "years")
    WebElement Year;
    @FindBy (id = "first_name")
    WebElement FirstName;
    @FindBy (id = "last_name")
    WebElement LastName;
    @FindBy (id = "company")
    WebElement Company;
    @FindBy (id = "address1")
    WebElement Address;
    @FindBy (id = "country")
    WebElement Country;
    @FindBy (id = "state")
    WebElement State;
    @FindBy (id = "city")
    WebElement City;
    @FindBy (id = "zipcode")
    WebElement Zipcode;
    @FindBy (id = "mobile_number")
    WebElement MobileNumber;
    @FindBy (xpath = "//button[normalize-space()='Create Account']")
    WebElement CreateBtn;
    @FindBy(xpath = "//b[normalize-space()='Account Created!']" )
    WebElement AccountCreated;
    @FindBy(xpath = "//a[@class='btn btn-primary']")
    WebElement Continue;
    @FindBy(xpath = "//a[normalize-space()='Logout']")
    WebElement Logout;

    public void fillRegisterationData(String name,String day, String month, String year,
                                      String password,String firstName, String lastName, String company,
                                      String address, String country, String state,String city,
                                      String zipCode,String mobileNumber){
        clickButton(SignUpLink);
        wait.until(ExpectedConditions.visibilityOf(Name)).sendKeys(name);
        wait.until(ExpectedConditions.visibilityOf(Email)).sendKeys(generateEmail());
        clickButton(SignUpBtn);
        wait.until(ExpectedConditions.visibilityOf(Gender)).click();
        wait.until(ExpectedConditions.visibilityOf(Password)).sendKeys(password);
        SelectFromLOV(Day,day);
        SelectFromLOV(Month,month);
        SelectFromLOV(Year,year);
        wait.until(ExpectedConditions.visibilityOf(FirstName)).sendKeys(firstName);
        wait.until(ExpectedConditions.visibilityOf(LastName)).sendKeys(lastName);
        wait.until(ExpectedConditions.visibilityOf(Company)).sendKeys(company);
        wait.until(ExpectedConditions.visibilityOf(Address)).sendKeys(address);
        SelectFromLOV(Country,country);
        wait.until(ExpectedConditions.visibilityOf(State)).sendKeys(state);
        wait.until(ExpectedConditions.visibilityOf(City)).sendKeys(city);
        wait.until(ExpectedConditions.visibilityOf(Zipcode)).sendKeys(zipCode);
        wait.until(ExpectedConditions.visibilityOf(MobileNumber)).sendKeys(mobileNumber);
        wait.until(ExpectedConditions.elementToBeClickable(CreateBtn)).click();
    }
    public void AssertAccountCreated(WebDriver driver)
    {
        String actualName = AccountCreated.getText();
        String expectedName = "ACCOUNT CREATED!";
        Assert.assertEquals(expectedName,actualName);
        System.out.println(actualName);
        wait.until(ExpectedConditions.elementToBeClickable(Continue)).click();
    }

    public void Logout()
    {
        wait.until(ExpectedConditions.elementToBeClickable(Logout)).click();
    }

}