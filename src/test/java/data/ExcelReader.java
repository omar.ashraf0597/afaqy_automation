package data;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ExcelReader {


    static FileInputStream fis =null;
    public FileInputStream getFileInputStream()
    {
        String filePath = System.getProperty("user.dir") + "/src/test/java/data/inputDataForRegisteration.xlsx";
        File srcFile = new File(filePath);
        try
        {
            fis = new FileInputStream(srcFile);
        }
        catch (FileNotFoundException e)
        {
            System.out.println("Error Occured");
            System.exit(0);
        }

        return fis;
    }

    public Object[][] getExcelData() throws IOException {
        fis = getFileInputStream();
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet sheet = wb.getSheetAt(0);

        int TotalNumberOfRows = (sheet.getLastRowNum()+1);
        int TotalNumberOfColumns = 14;

        String[][] arrayExcelData = new String[TotalNumberOfRows][TotalNumberOfColumns];
        for (int i = 0; i < TotalNumberOfRows; i++)
        {
            for (int y = 0; y < TotalNumberOfColumns; y++) {
                XSSFRow row = sheet.getRow(i);
                arrayExcelData[i][y] = row.getCell(y).toString();
            }
        }
        wb.close();
        return arrayExcelData;
    }



}
