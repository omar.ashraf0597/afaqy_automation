package Tests;
import Pages.SignUpPage;
import data.ExcelReader;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;


public class SignUpTest extends BaseTest {
    SignUpPage signUp;
    @DataProvider(name = "DataFromExcel")
    public Object[][] InputData() throws IOException {
        ExcelReader er = new ExcelReader();
        return er.getExcelData();
    }

    @Test (alwaysRun = true, priority = 1, groups = "FirstTimeOpenURL", dataProvider = "DataFromExcel")
    public void Registration(String name,String day, String month, String year,
                             String password,String firstName, String lastName, String company,
                             String address, String country, String state,String city,
                             String zipCode,String mobileNumber) {
        signUp = new SignUpPage(driver);
        signUp.fillRegisterationData(name,day,month,year,password,firstName,lastName,company,
                address,country,state,city,zipCode,mobileNumber);
        signUp.AssertAccountCreated(driver);
    }

    @Test(alwaysRun = true, priority = 2, dependsOnMethods = {"Registration"})
    public void Logout() {
        signUp= new SignUpPage(driver);
        signUp.Logout();
    }
}


