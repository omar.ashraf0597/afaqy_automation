package Tests;

import java.util.concurrent.TimeUnit;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.OperatingSystem;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;

public class BaseTest {

    public static WebDriver driver;

    @BeforeTest
    public void StartDriver() {
        WebDriverManager.chromedriver().operatingSystem(OperatingSystem.WIN).setup();
    }
    @BeforeMethod(onlyForGroups = "FirstTimeOpenURL")
    public void Navigation() {
        ChromeOptions options = new ChromeOptions();
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        driver.get("https://automationexercise.com/");
    }

    @AfterTest
    public void stopDriver() {
        driver.quit();
    }
}
